import { Invitation, BackgroundJob } from '../../entities';
import { axios, runCatching } from '../util';

export async function createInvitation(
  firstname: string,
  surname: string,
  mail: string,
  groups: number[],
  permissions: string[]
): Promise<Invitation | string[]> {
  return await runCatching<Invitation | string[]>(() =>
    axios.post(
      `/invitations/`,
      {
        firstname,
        surname,
        mail,
        groups,
        permissions,
        instructed: true,
        verified: true,
      },
      {
        validateStatus: (it) => it == 201 || it == 400,
      }
    )
  ).then((it) => it.data);
}

export async function getInvitation(id: string): Promise<Invitation | undefined> {
  return await runCatching<Invitation>(() =>
    axios.get(`/invitations/${id}`, {
      validateStatus: (it) => it == 200 || it == 404 || it == 400,
    })
  ).then((it) => (it.status == 200 ? it.data : undefined));
}

export async function repeatInvitation(
  invitation: Invitation
): Promise<'success' | 'not_found' | 'no_permission'> {
  return await runCatching(() =>
    axios.post(
      `/invitations/resend`,
      {
        mail: invitation.invitee.mail,
        verified: true,
      },
      {
        validateStatus: (it) => it == 200 || it == 404 || it == 403,
      }
    )
  ).then((it) => {
    switch (it.status) {
      case 200:
        return 'success';
      case 404:
        return 'not_found';
      case 403:
        return 'no_permission';
      default: {
        throw new Error('This error should not be possible');
      }
    }
  });
}

export async function withdrawInvitation(
  invitation: Invitation
): Promise<'success' | 'not_found' | 'no_permission'> {
  return await runCatching(() =>
    axios.post(
      `/invitations/withdraw`,
      {
        mail: invitation.invitee.mail,
        verified: true,
      },
      {
        validateStatus: (it) => it == 200 || it == 404 || it == 403,
      }
    )
  ).then((it) => {
    switch (it.status) {
      case 200:
        return 'success';
      case 404:
        return 'not_found';
      case 403:
        return 'no_permission';
      default: {
        throw new Error('This error should not be possible');
      }
    }
  });
}

export async function getInvitations(): Promise<Invitation[]> {
  return await runCatching<Invitation[]>(() =>
    axios.get(`/invitations`, {
      validateStatus: (it) => it == 200,
    })
  ).then((it) => it.data);
}

export async function consumeInvitation(
  id: string,
  password: string,
  displayName: string,
  gdprId?: number
): Promise<{
  state: 'success' | 'token_not_found' | 'error';
  messages: string[];
  successData: BackgroundJob | undefined;
}> {
  return await runCatching<string[] | { id: string }>(() =>
    axios.post(
      `/invitations/${id}`,
      {
        password1: password,
        password2: password,
        displayName: displayName,
        gdpr: gdprId,
      },
      {
        validateStatus: (it) => it == 201 || it == 404 || it == 400,
        params: {
          async: true,
        },
      }
    )
  ).then((it) => ({
    state: it.status == 404 ? 'token_not_found' : it.status == 201 ? 'success' : 'error',
    messages: it.status == 400 && Array.isArray(it.data) ? it.data : [],
    successData: it.status == 201 ? (it.data as BackgroundJob) : undefined,
  }));
}
