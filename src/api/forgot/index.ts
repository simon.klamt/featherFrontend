import { PasswordReset } from '../../entities';
import { axios, runCatching } from '../util';

export async function initPasswordReset(mail: string): Promise<{}> {
  return await runCatching<{}>(() =>
    axios.post(`/forgot/step1/${mail}`, null, {
      validateStatus: (it) => it == 201,
    })
  ).then((it) => it.data);
}

export async function getPasswordReset(token: string): Promise<PasswordReset | undefined> {
  return await runCatching<PasswordReset>(() =>
    axios.get(`/forgot/step2/${token}`, {
      validateStatus: (it) => it == 200 || it == 404 || it == 400,
    })
  ).then((it) => (it.status == 200 ? it.data : undefined));
}

export async function executePasswordReset(
  token: string,
  password: string
): Promise<{
  state: 'success' | 'token_not_found' | 'error';
  messages: string[];
}> {
  return await runCatching<string[]>(() =>
    axios.post(
      `/forgot/step2/${token}`,
      {
        password1: password,
        password2: password,
      },
      {
        validateStatus: (it) => it == 204 || it == 404 || it == 400,
      }
    )
  ).then((it) => ({
    state: it.status == 404 ? 'token_not_found' : it.status == 204 ? 'success' : 'error',
    messages: it.status == 400 ? it.data : [],
  }));
}
