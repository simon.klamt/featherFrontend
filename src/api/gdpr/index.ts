import { GdprDocument } from '../../entities';
import { axios, runCatching } from '../util';

export async function getGdprDocument(
  version: number | 'latest'
): Promise<GdprDocument | undefined> {
  return await runCatching<GdprDocument>(() =>
    axios.get(`/gdpr/documents/${version}`, {
      validateStatus: (it) => it == 200 || it == 404,
    })
  ).then((it) => (it.status == 404 ? undefined : it.data));
}
