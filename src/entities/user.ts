export type User = {
  id: number;
  username: string;
  displayName: string;
  firstname: string;
  surname: string;
  mail: string;
  groups: string[];
  registeredSince: string;
  ownedGroups: string[];
  permissions: string[];
  disabled: boolean;
  lastLoginAt: string;
};
