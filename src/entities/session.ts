import { User } from './user';

export type Session = {
  id: string;
  validUntil: string;
  user: User;
  authorizer: string;
  miniSession: boolean;
};
