export type Group = {
  id: number;
  name: string;
  description: string;
  groupMembers: number[];
  ownedGroups: number[];
  ownerGroups: number[];
  owners: number[];
  parentGroups: number[];
  userMembers: number[];
};
