import { User } from './user';

export type Invitation = {
  id: string;
  inviter: User;
  validUntil: string;
  used: boolean;
  invitee: User;
};
