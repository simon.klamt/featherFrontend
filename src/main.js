/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import Vue from 'vue';
import pinia from './stores';
import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';
import GroupListEntry from './components/GroupListEntry.vue';
import GroupList from './components/GroupList.vue';
import InvitationList from './components/InvitationList.vue';
import BackgroundJobStatus from './components/BackgroundJobStatus.vue';
import FileUpload from './components/FileUpload.vue';
import ErrorAlert from './components/ErrorAlert.vue';

Vue.config.productionTip = false;

// component registration
Vue.component('group-list-entry', GroupListEntry);
Vue.component('group-list', GroupList);
Vue.component('invitation-list', InvitationList);
Vue.component('group-list-entry', GroupListEntry);
Vue.component('background-job-status', BackgroundJobStatus);
Vue.component('file-upload', FileUpload);
Vue.component('error-alert', ErrorAlert);

new Vue({
  pinia,
  router,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
