export { CONFIG } from './config';
export { validateMail } from './mail';
export { validatePassword, validatePasswordsEqual } from './password';
export {
  validateDisplayName,
  suggestDisplayName,
  userIdentifier,
  userUniqueIdentifier,
  validateName,
} from './displayName';
