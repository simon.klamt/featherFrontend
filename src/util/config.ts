const dataset = document.getElementById('app')?.dataset;

var config = {
  VENDOR_NAME: dataset?.vendor ?? 'VENDOR',
  API_BASE_URL: dataset?.apiBaseUrl ?? 'API_BASE_URL',
  BASE_URL: dataset?.baseUrl ?? 'BASE_URL',
  LOGO_URL: dataset?.logoUrl ?? 'LOGO_URL',
  MAIN_BAR_COLOR: dataset?.mainBarColor ?? 'MAIN_BAR_COLOR',
  QUICK_START_GUIDE_URL: dataset?.quickStartGuideUrl ?? 'QUICK_START_GUIDE_URL',
};

if (import.meta.env.DEV) {
  config = {
    VENDOR_NAME: import.meta.env.VITE_VENDOR_NAME ?? 'LOCAL DEV',
    API_BASE_URL: import.meta.env.VITE_API_BASE_URL ?? 'http://localhost:7000',
    BASE_URL: import.meta.env.VITE_BASE_URL ?? 'http://localhost:3000',
    LOGO_URL: import.meta.env.VITE_LOGO_URL ?? 'https://maximilian.dev/assets/images/favicon.svg',
    MAIN_BAR_COLOR: import.meta.env.VITE_MAIN_BAR_COLOR ?? '#db3a17',
    QUICK_START_GUIDE_URL: import.meta.env.VITE_QUICK_START_GUIDE_URL ?? 'https://maximilian.dev',
  };
}

export const CONFIG = config;
