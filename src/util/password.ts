export function validatePassword(password?: string): string | boolean {
  if (!password) return 'Bitte ein Passwort eingeben';
  if (password.length < 8) return 'Ein Passwort muss mindestens 8 Zeichen enthalten';
  if (password.length > 400) return 'Ein Passwort darf höchsten 400 Zeichen enthalten';
  return true;
}

export function validatePasswordsEqual(password1?: string, password2?: string): string | boolean {
  return password1 === password2 ? true : 'Passwörter müssen übereinstimmen';
}
