FROM flashspys/nginx-static:latest@sha256:7834ae32f1ea9b108fa5b467e0afb51507e92e7eb68e6593c19453978c61e297
COPY dist /static
COPY default.conf /etc/nginx/conf.d/default.conf
COPY entrypoint.sh /entrypoint.sh

# RUN nginx in non privileged mode and make this image openshift compatible
RUN addgroup nginx root && \
    chmod g+rwx /var/cache/nginx /run && \
    chgrp -R root /var/cache/nginx && \
    sed -i.bak 's/listen\(.*\)80;/listen 8080;/' /etc/nginx/conf.d/default.conf && \
    sed -i.bak 's/^user/#user/' /etc/nginx/nginx.conf && \
    chown nginx:root -R /static && \
    chmod g+w /static/index.html

USER nginx
EXPOSE 8080

ENV VENDOR_NAME "Feather"
ENV API_BASE_URL "https://feather.maximilian.dev"
ENV LOGO_URL "https://maximilian.dev/logo.png"
ENV MAIN_BAR_COLOR "orange"
ENV QUICK_START_GUIDE_URL "https://openproject.apps.ingenieure.cloud/projects/ingenieure-ohne-grenzen/wiki/1-quickstart-guide"
ENV BASE_URL "https://service.maximilian.dev"

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
